<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>JQuery サンプル</title>
      <script src="//code.jquery.com/jquery-3.2.1.min.js"></script>
      <script src="js/script.js"></script>
      <link href="./css/style.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <label>名前</label>
    <input type='text'>
    <button id="hoge">HOGEボタン</button>
    <button class="fuga">FUGAボタン</button>
    <div>
      <span>
        <h1>Divの中のSpanの中のH1です。</h1>
      </span>
    </div>
    <p>jQueryのthisについて1</p>
    <p>jQueryのthisについて2</p>
    <p>jQueryのthisについて3</p>
    <p>jQueryのthisについて4</p>
  </body>
</html>
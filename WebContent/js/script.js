$("input[type='text']")

$('#hoge')

$('.fuga')

$('div span h1')

// <p>要素をクリックしたら発動
$('p').click(function() {
  // 全ての<p>要素の文字色を赤字にする
  $('p').css('color', 'red');
});

/*$(function() {
	  // id='hoge'が設定されたボタンをクリックしたらダイアログを表示する。
	  $('#hoge').on('click', function() {
	    alert('pushed the HOGE button!!');
	  });
	});

	$(document).ready(function(){
	  // id='fuga'が設定されたボタンをクリックしたらダイアログを表示する。
	  $('#fuga').on('click', function() {
	    alert('pushed the FUGA button!!');
	  });
	});

	$(document).ready(function() {
	  // id='piyo'が設定されたボタンをクリックしたらダイアログを表示する。
	  $('#piyo').on('click', function() {
	    alert('pushed the PIYO button!!');
	  });
	});*/

$(document).ready(function() {
	  // id='hoge'が設定されたボタンをクリックしたらダイアログを表示する。
	  $('#hoge').on('click', function() {
	    alert('pushed the HOGE button!!');
	  });
	  // id='fuga'が設定されたボタンをクリックしたらダイアログを表示する。
	  $('.fuga').on('click', function() {
	    alert('pushed the FUGA button!!');
	  });
	  // id='piyo'が設定されたボタンをクリックしたらダイアログを表示する。
	  $('#piyo').on('click', function() {
	    alert('pushed the PIYO button!!');
	});
});

//「OK」を選択したらリンク先に遷移するメソッド
function jumpConfirm(obj) {
  // イベントが設定されたタグの情報を引数「this」で渡しており、タグの情報を使える
  // 「https://www.alhinc.jp/　に遷移しますか？」と確認ダイアログが表示される
  if (confirm(obj.href + "　に遷移しますか？")) {
    return true;
  	}
  	return false;
}

$('div').on({
	  mouseenter: function() {
	    // マウスが要素上に入った時の処理
	    $(this).css('background', '#00bdb7');
	  },
	  mouseleave: function() {
	    // マウスが要素上から離れた時の処理
	    $(this).css('background', '#ccc');
	  },
	  click: function() {
	    // クリックした時の処理
	    alert('クリック！！！');
	  }
	});


$('div').on('click', { name: 'マサルくん' }, function(event) {
	  // event.dataにnameの値が格納される
	  alert(event.data.name);
	});


$('div').on({
	  mouseenter: function() {
	    $(this).animate({ opacity: 0.5 }, 300, 'swing');
	  },
	  mouseleave: function() {
	    $(this).animate({ opacity: 1 }, 300, 'swing');
	  }
	});

$('div').click(function() {
	  // divをクリックするとdivの中にあるpの要素の色が変わる
	  $(this).find('p').css('color', '#fcd');
	});

$('p').click(function() {
    $(this).parent().css('background-color', '#000');
});

$('button').click(function() {
	  $('div').text('書き換えたよ');
	});


$(function() {
    // 登録ボタンを押したら
    $('button#register').on('click', function() {
        // 送信するデータを用意
        var codeDate = $('#code').val();
        var nameDate = $('#name').val();
        var user = { 'code': codeDate, 'name': nameDate };

        // Ajax通信処理
        $.ajax({
            // レスポンスをJSONとしてパースする
            dataType: 'json',
            // POSTで通信する
            type: 'POST',
            // POST送信先のURL
            url: 'utsugi.kenta/register',
            // JSONデータ本体
            data: { user: JSON.stringify(utsugi.kenta) }
        }).done(function(data) {
            // 成功時の処理
            console.log('success!!');
        }).fail(function(data) {
            // 失敗時の処理
            console.log('error!!');
        });
    });
});
